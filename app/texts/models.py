from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from ckeditor_uploader.fields import RichTextUploadingField

from app.general.models import Seo


class Text(Seo):
    title = models.CharField(
        max_length=300,
        verbose_name=_('Title'),
        unique=True
    )
    description = RichTextUploadingField(
        verbose_name=_('Description'),
        blank=True
    )

    class Meta(Seo.Meta):
        verbose_name = _("Text")
        verbose_name_plural = _("Texts")

    def __str__(self):
        return str(self.title)

    def url(self):
        kwargs = {
            "text" : self.slug
        }
        return reverse('text-item', kwargs=kwargs)
