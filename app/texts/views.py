from django.views.generic import TemplateView
from django.http import Http404

from app.texts.models import Text


class TextItemView(TemplateView):
    template_name = 'text-item.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['text'] = Text.objects.filter(slug=kwargs['text']).first()
        if not context['text']:
            raise Http404
        return context
