from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationAdmin, TranslationStackedInline

from app.texts.models import *
from app.general.admin_helper import *


#
# Text
#

@admin.register(Text)
class TextAdmin(TranslationAdmin):
    fieldsets = (
        ('', {'fields':  ('title', 'description')}),
        seo_fieldset
    )
