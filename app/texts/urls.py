from django.urls import path
from django.utils.translation import gettext, gettext_lazy as _

from app.texts.views import *


urlpatterns = [
    path(_('legal/<text>/'), TextItemView.as_view(), name='text-item'),
]