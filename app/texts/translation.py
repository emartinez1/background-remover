from modeltranslation.translator import translator, TranslationOptions

from app.texts.models import *
from app.general.translation_helper import *


class TextTranslation(TranslationOptions):
    fields = ('title', 'description',) + seo_fields
translator.register(Text, TextTranslation)
