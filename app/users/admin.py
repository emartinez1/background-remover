from django.utils.safestring import mark_safe
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMultiAlternatives

from app.users.models import User
from app.general.models import Notification



def send_email(modeladmin, request, queryset):
    user_invitation = Notification.objects.filter(action='user-invitation').first()
    if user_invitation:
        for user in queryset:
            user_invitation.send(user, email_receiver=user.email)
send_email.short_description = _("Send invitation emails")



@admin.register(User)
class UserAdmin(BaseUserAdmin):
    actions = (
        send_email,
    )
    list_display = (
        'email',
        'is_superuser',
        'is_active',
        'last_login',
        'email_sent'
    )
    list_filter = (
        'is_superuser',
        'is_active',
        'email_sent'
    )
    search_fields = (
        'email',
    )
    ordering = ('email',)

    fieldsets = (
        (
            "",
            {
                'fields': (
                    'email',
                    'password',
                    'is_superuser',
                    'is_active',
                    'user_role',

                )
            }
        ),
    )
    add_fieldsets = (
        (
            "",
            {
                'classes': ('wide',),
                'fields': (
                    'email',
                    'password1',
                    'password2',
                    'is_superuser',
                    'user_role',

                )
            }
        ),
    )
