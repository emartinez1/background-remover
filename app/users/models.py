from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_superuser, **extra_fields):
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            is_superuser=is_superuser,
            is_active=True,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, **extra_fields):
        return self._create_user(
            email,
            password,
            False,
            **extra_fields
        )

    def create_superuser(self, email, password, **extra_fields):
        user = self._create_user(
            email,
            password,
            True,
            **extra_fields
        )
        return user


class User(AbstractBaseUser, PermissionsMixin):
    USER_ROLE_CHOICES = (
        ('SPECTATOR', _('Spectator')),
        ('PRESENTER', _('Presenter')),
    )
    username = None
    email = models.EmailField(
        max_length=254,
        unique=True,
        verbose_name=_("email")
    )

    is_superuser = models.BooleanField(
        default=False,
        verbose_name=_("Is admin?")
    )
    is_active = models.BooleanField(
        default=True,
        verbose_name=_("Active")
    )

    last_login = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("Last login")
    )
    date_joined = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Creation date")
    )
    user_role = models.CharField(
        choices=USER_ROLE_CHOICES,
        default="SPECTATOR",
        max_length=250,
    )
    email_sent = models.BooleanField(
        default=False,
        verbose_name=_("Email sent")
    )

    USERNAME_FIELD = 'email'
    # EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = [
    ]

    objects = UserManager()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    @property
    def is_staff(self):
        return self.is_superuser
