from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'app.users'
    verbose_name = 'Users'

    def ready(self):
        import app.users.signals
