function plyrstart(id){
    document.addEventListener('DOMContentLoaded', () => {
        const playerEl = document.getElementById(id);
        
        const player = new Plyr(playerEl,{
            captions: {
                //-active: true, 
                update: true, 
                //-language: 'en'
            },
            settings: ['captions', 'quality'],
            controls: [
                'play-large',
                // 'restart',
                // 'rewind',
                'play',
                // 'fast-forward',
                'progress',
                'current-time',
                // 'duration',
                'mute',
                'volume',
                'captions',
                'settings',
                'pip',
                'airplay',
                // 'download',
                'fullscreen',
            ],
            tooltips: { 
                controls: true, 
                seek: true 
            },
            i18n: {
                restart: 'Reiniciar',
                rewind: 'Rebobinar {seektime}s',
                play: 'Reproducir',
                pause: 'Pausar',
                fastForward: 'Avanzar {seektime}s',
                seek: 'Buscar',
                seekLabel: '{currentTime} of {duration}',
                played: 'Reproducido',
                buffered: 'En memoria',
                currentTime: 'Tiempo actual',
                duration: 'Duración',
                volume: 'Volumen',
                mute: 'Silenciar',
                unmute: 'Dejar de silenciar',
                enableCaptions: 'Activar subtítulos',
                disableCaptions: 'Desactivar subtítulos',
                download: 'Descargar',
                enterFullscreen: 'Pantalla completa',
                exitFullscreen: 'Salir de la pantalla completa',
                frameTitle: 'Reproductor de {title}',
                captions: 'Subtítulos',
                settings: 'Configuración',
                pip: 'Minireproductor',
                menuBack: 'Ir atrás al menú anterior',
                speed: 'Velocidad',
                normal: 'Normal',
                quality: 'Calidad',
                loop: 'Bucle',
                start: 'Inicio',
                end: 'Fin',
                all: 'Todo',
                reset: 'Reiniciar',
                disabled: 'Desactivado',
                enabled: 'Activado',
                advertisement: 'Anuncio',
                qualityBadge: {
                2160: '4K',
                1440: '2K',
                1080: 'FHD',
                720: 'HD',
                576: 'SD',
                480: 'SD',
                },
            },
        });
        // Expose player so it can be used from the console
        window.player = player;
        window.playerEl = playerEl;
    });
}
function plyrset(video){
    document.addEventListener('DOMContentLoaded', () => {
        if (video.autoplay == true){
            player.once('canplay', event => {
                player.play();
            });
        } else {console.log("testas")}
        if (video.url) {
            if (!Hls.isSupported()) {
                playervideo.src = video.url;
            } else {
                // For more Hls.js options, see https://github.com/dailymotion/hls.js
                const hls = new Hls();
                hls.loadSource(video.url);
                hls.attachMedia(playerEl);
                window.hls = hls;
                
                // Handle changing captions
                player.on('languagechange', () => {
                    // Caption support is still flaky. See: https://github.com/sampotts/plyr/issues/994
                    setTimeout(() => hls.subtitleTrack = player.currentTrack, 50);
                });
            }
            
        } else {
            console.log("Error: No source")
        }
        if (video.poster) {
            player.once('ready', event => {
                player.poster = video.poster;
            });
        } else {
            console.log("Alert: No poster")
        }
        if (video.start) {
            player.once('canplay', event => {
                player.currentTime = video.start;
            });
        } else {
            console.log("Alert: No start")
        }
        
    });
}
function plyr(id, video){
    plyrstart(id);
    plyrset(video);
}