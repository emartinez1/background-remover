function interFire(id){
    document.body.classList.add("up-inter--on");
    var el = document.getElementById(id);
    el.classList.add("up-inter_wrap--on")
    preventOut();
}

function modalFire(id){
    document.body.classList.add("up-modal--on");
    var el = document.getElementById(id);
    el.classList.add("up-modal_wrap--on")
}
function modalUnfire(id){
    document.body.classList.remove("up-modal--on");
    var el = document.getElementById(id);
    el.classList.remove("up-modal_wrap--on")
}
function preventOut(){
    window.onbeforeunload = function() { return "Your work will be lost."; };
}
function preventOutOff(){
    window.onbeforeunload = function() {};
}

function codeCheck(id){
    var container = document.getElementById(id);
    container.onkeyup = function(e) {
        var target = e.srcElement || e.target;
        var maxLength = parseInt(target.attributes["maxlength"].value, 10);
        var myLength = target.value.length;
        if (myLength >= maxLength) {
            var next = target;
            while (next = next.nextElementSibling) {
                if (next == null)
                    break;
                if (next.tagName.toLowerCase() === "input") {
                    next.focus();
                    break;
                }
            }
        }
        else if (myLength === 0) {
            var previous = target;
            while (previous = previous.previousElementSibling) {
                if (previous == null)
                    break;
                if (previous.tagName.toLowerCase() === "input") {
                    previous.focus();
                    break;
                }
            }
        }
    }
}
function psShow(id){
    let password = document.getElementById(id)
    if(password){
        let father = password.parentElement
        let eye = father.getElementsByClassName("fo-iw-icon")[0]
        eye.addEventListener("click", () => {
            if (password.type === "password") {
                password.type = "text";
                eye.classList.add("fo-iw-icon--off");
            } else {
                password.type = "password";
                eye.classList.remove("fo-iw-icon--off");
            }
        });
    }
}
function psCheck(id){
    let password = document.getElementById(id)
    if(password){

        let timeout;
        let father = password.parentElement
        let strengthText = father.getElementsByClassName("fo-psw_str__text")[0]
        let eye = father.getElementsByClassName("fo-iw-icon")[0]

        let strongPassword = new RegExp('(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{10,})')
        let goodPassword = new RegExp('((?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,}))|((?=.*[^A-Za-z0-9])(?=.{10,}))')
        let mediumPassword = new RegExp('((?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[^A-Za-z0-9])(?=.{8,}))')
        
        function StrengthChecker(PasswordParameter){
            if(strongPassword.test(PasswordParameter)) {
                father.setAttribute("strength", "strong")
                strengthText.textContent = 'Genial'
            } else if(goodPassword.test(PasswordParameter)){
                father.setAttribute("strength", "good")
                strengthText.textContent = 'Bien'
            } else if(mediumPassword.test(PasswordParameter)){
                father.setAttribute("strength", "medium")
                strengthText.textContent = 'Regular'
            }else{
                father.setAttribute("strength", "bad")
                strengthText.textContent = 'Insuficiente'
            }
        }

        window.onload = function() {
            let att = document.createAttribute("strength");
            password.setAttributeNode(att);
            StrengthChecker(password.value);
        };
        password.addEventListener("input", () => {
            clearTimeout(timeout);
            timeout = setTimeout(() => StrengthChecker(password.value), 500);
        });

    }
}