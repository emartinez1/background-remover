function coverFAQ(){
    const faq = document.getElementsByClassName("co-faq-item")
    for (var i = 0; i < faq.length; i++) {
        const q = faq.item(i)
        const qh = q.getElementsByClassName("co-faq-item-question")[0]
        qh.addEventListener("click", function(){
            q.classList.toggle("co-faq-item--active");
        })
    }
}
function slider(){
    const sliderList = document.getElementsByClassName("slider_img")
    for (var i = 0; i < sliderList.length; i++) {
        
        const slider = sliderList.item(i);
        const fore = slider.querySelector("img:first-child");

        const btn = document.createElement("div");
        btn.className = "slider_img__btn"
        slider.appendChild(btn);

        const bar = document.createElement('input');
        bar.type = "range";
        bar.className = "slider_img__range"
        bar.min = 0;
        bar.max = 100;
        bar.value = 50;
        slider.appendChild(bar);

        bar.addEventListener("input", function(){
            let position = bar.value
            fore.style.width =  position + "%";
            btn.style.left = position + "%";
        })
        
    }
}