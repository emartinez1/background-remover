function lswipper(){
    const swiper = new Swiper('.vg-list', {
        slidesPerGroup: 1,
        slidesPerView: "1.3",
        spaceBetween: 20,
        freeMode: true,
        grabCursor: true,
        breakpoints: {
            640: {
                slidesPerGroup: 2,
                slidesPerView: "2.3",
                freeMode: false,
            },
            1024: {
                slidesPerView: "3.3",
                slidesPerGroup: 3,
                freeMode: false,
            },
            1440: {
                slidesPerView: "4.3",
                slidesPerGroup: 4,
                freeMode: false,
            },
            1920: {
                slidesPerView: "5.3",
                slidesPerGroup: 5,
                freeMode: false,
            },
        },
        navigation: {
            nextEl: '.vg-list-act--next',
            prevEl: '.vg-list-act--prev',
            disabledClass: 'vg-list-act--disabled',
            hiddenClass: 'vg-list-act--hidden',
            lockClass: 'vg-list-act--lock',
        },
        wrapperClass: "vg-list-wrapper",
        slideClass: "ve",
        slideActiveClass: "ve--active",
        slideNextClass: "ve--next",
        slidePrevClass: "ve--prev",
        notificationClass: "ve__notification",
    });
};