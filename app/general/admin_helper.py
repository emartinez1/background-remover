from django.conf import settings

seo_fieldset = ("SEO", {'classes': ('grp-collapse grp-closed',), "fields" : ('slug', 'seo_title', 'seo_description')})


def prepopulated_field_translated(field, source_field):
    fields = []
    for code, name in settings.LANGUAGES:
        code = code.replace("-", "_")
        
        fields += [("{}_{}".format(field, code), ("{}_{}".format(source_field, code),))]
    return fields