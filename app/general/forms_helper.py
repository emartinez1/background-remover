from django.utils.translation import ugettext_lazy as _


class FormWithPlaceholders:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field.label:
                placeholder = _('Introduce tu ') + field.label.lower()
                field.widget.attrs.update({'placeholder': placeholder})


class FormWithUniqueFields:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field.label:
                name = str(field.label).lower()
                field.error_messages['unique'] = _(
                    "Este %(name)s ya está en uso. Por favor, elige uno diferente."
                ) % {'name': name}
