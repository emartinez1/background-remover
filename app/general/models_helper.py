from django.conf import settings
from django.utils.text import slugify
from django.utils.html import strip_tags
from django.http import HttpRequest

import html
import six
import inspect


def seo_fields_populate(object):
    fields = (
        ("slug", ['title', 'name'], 60),
        ("seo_title", ['title', 'name'], 60),
        ("seo_description", ['introduction', 'description', 'title', 'name'], 160),
    )

    for field, population_fields, max_length in fields:
        for code, name in settings.LANGUAGES:
            # Replace - and _ for been able to look up fields of zh-hans
            code = code.replace("-", "_")

            # Check if the field doesn't have values
            if not getattr(object, field + '_' + code):
                for population_field in population_fields:
                    try:
                        # Get the value of the population field and quit html tags
                        population_field_content = str(html.unescape(strip_tags(getattr(object, population_field + '_' + code))))
                        if population_field_content and population_field_content != "None":
                            if field == 'slug':
                                population_field_content = slugify(population_field_content)
                                
                                # Make sure that it isn't a duplicated slug
                                object_kwargs = {
                                    'slug_' + code : population_field_content
                                }
                                if object.__class__.objects.filter(**object_kwargs).exclude(pk=object.pk):
                                    population_field_content += "-{}".format(object.pk)
                            
                            # Set the field value sliced at max length
                            setattr(object, field + '_' + code, population_field_content[:max_length])
                            break
                    except:
                        next
                

def slug_protector(object, slug_field="slug"):
    prepopulate_slug = getattr(object, slug_field + '_' + settings.MODELTRANSLATION_DEFAULT_LANGUAGE)

    for code, name in settings.LANGUAGES:
        # Replace - and _ for been able to look up fields of zh-hans
        code = code.replace("-", "_")
        slug = getattr(object, slug_field + '_' + code)

        if not slug:
            setattr(object, slug_field + '_' + code, prepopulate_slug)
