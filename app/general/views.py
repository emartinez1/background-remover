from django.shortcuts import render
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

import importlib


#
# Errors
#

def error_404(request, exception=None, template_name="error.html"):
    response = render(
        request,
        "error.html",
        context=RequestContext(
            request,
            {
                'error_code': 404,
                'error_name': _('Página no encontrada'),
                'error_description': _('Lo sentimos, la página que estás buscando no existe.'),
            },
        ).dict,
        status=404,
    )
    return response


def error_500(request, template_name="error.html"):
    response = render(
        request,
        "error.html",
        context=RequestContext(
            request,
            {
                'error_code': 500,
                'error_name': _('Error del servidor'),
                'error_description': _('¡Ups! Algo salió mal. Inténtalo de nuevo en unos minutos.'),

            },
        ).dict,
        status=500,
    )
    response.status_code = 500
    return response


def error_403(request, exception, template_name="error.html"):
    response = render(
        request,
        "error.html",
        context=RequestContext(
            request,
            {
                'error_code': 403,
                'error_name': _('Permiso denegado'),
                'error_description': _('Lo siento, no tienen acceso a esta página.'),

            }
        ).dict,
        status=403,
    )
    return response


def error_400(request, exception, template_name="error.html"):
    response = render(
        request,
        "error.html",
        context=RequestContext(
            request,
            {
                'error_code': 400,
                'error_name': _('Solicitud incorrecta'),
                'error_description': _('Lo siento, esta es una petición equivocada.'),

            }
        ).dict,
        status=400,
    )
    return response

class RequestContext:
    """
    Get the values of all context_processors.
    Django has a RequestContext class but is broken.
    """

    def __init__(self, request, initial_data):
        self.request = request
        self.initial_data = initial_data
        self.dict = self.get_dict()

    def get_dict(self):
        _dict = {}
        context_processors = self.get_context_processors()
        for context_processor in context_processors:
            value = self.get_context_processor_value(context_processor)
            for key, value in value.items():
                _dict[key] = value

        self.add_inital_data(_dict)
        return _dict

    def get_context_processors(self):
        context_processors = settings.TEMPLATES[0]['OPTIONS']['context_processors']
        # Exclude django context_processors because isn't needed
        context_processors = list(
            filter(
                lambda context_processor: 'django' not in context_processor,
                context_processors
            )
        )
        return context_processors

    def get_context_processor_value(self, context_processor):
        module_name, function_name = context_processor.rsplit('.', 1)
        module = importlib.import_module(module_name)
        function = getattr(module, function_name)
        return function(self.request)

    def add_inital_data(self, _dict):
        for key, value in self.initial_data.items():
            _dict[key] = value
