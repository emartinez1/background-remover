from django.db import models
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.template import Context, Template
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from app.users.models import User


import html
from solo.models import SingletonModel
from filebrowser.fields import FileBrowseField

from app.general.models_helper import *


#
# General
#

class General(SingletonModel):
    logo = FileBrowseField(
        verbose_name=_("Logo"), 
        max_length=250,
        null=True,
        blank=True,
    )
    favicon = FileBrowseField(
        verbose_name=_("Favicon web"), 
        max_length=250, 
        blank=True,
        default=""
        
    )
    company_name = models.CharField(
        max_length=250,
        blank=True,
        verbose_name=_("Company name"), 
    )
    site_name = models.CharField(
        max_length=250,
        blank=True,
        verbose_name=_("Nombre web"), 
    )
    privacy_policy = models.ForeignKey(
        'texts.Text',
        verbose_name=_("Privacy Policy"),
        related_name="privacy_policy",
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    terms_and_conditions = models.ForeignKey(
        'texts.Text',
        verbose_name=_("Terms and conditions"),
        related_name="terms_and_conditions",
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    # cookies_policy = models.ForeignKey(
    #     'texts.Text',
    #     verbose_name=_("Política de cookies"),
    #     related_name="cookies_policy",
    #     on_delete=models.SET_NULL,
    #     null=True,
    #     blank=True
    # )
    
    def __str__(self):
        return "General"

    class Meta:
        verbose_name = "General"

#
# Models inherance
#

class Seo(models.Model):
    HELP_TEXT = "Si el campo está vacío, se generará automáticamente"
    slug = models.SlugField(verbose_name=_("Slug"), max_length=60, unique=True)
    seo_title = models.CharField(verbose_name=_("Title"), max_length=60, blank=True)
    seo_description = models.TextField(verbose_name=_("Description"), max_length=160, blank=True)

    class Meta:
        abstract = True

    def save(self, **kwargs):
        # seo_fields_populate(self)
        # slug_protector(self)
        super(Seo, self).save(**kwargs)


class Order(models.Model):
    order = models.IntegerField(
        verbose_name='orden',
        default=1
    )

    class Meta:
        abstract = True
        ordering = ['order']



class Notification(models.Model):
    ACTION_CHOICES = [
        ('user-invitation', _('Invitation email')),
    ]
    action = models.CharField(max_length=250, verbose_name=_("Acción"), choices=ACTION_CHOICES,)

    # Email
    email_subject = models.CharField(max_length=250, verbose_name=_("Asunto"), blank=True)
    email_content = RichTextField(verbose_name=_("Contenido"), blank=True)
    email_active = models.BooleanField(default=True, verbose_name=_("Activo"))

    def __str__(self):
        return self.get_action_display()
        
    def send(self, _object={}, email_receiver=""):
        if self.email_active:
            log_kwargs = {
                'content_object': _object,
                'notification': self,
                'email_status': 'success'
            }
            if settings.DEBUG:
                email_receiver = settings.TEST_EMAIL
            try:
                context = Context({'tags' : _object})
                content = Template(html.unescape(self.email_content)).render(context=context)

                subject = Template(self.email_subject).render(context=context)

                html_content = render_to_string('emails/base.html', {'subject': subject, 'content' : content})
                text_content = strip_tags(content)

                msg = EmailMultiAlternatives(subject, text_content, settings.DEFAULT_FROM_EMAIL, [email_receiver])
                msg.attach_alternative(html_content, "text/html")

                # msg.send()
                user = User.objects.get(pk=_object.pk)
                user.email_sent = True
                user.save()

            except Exception as e:
                log_kwargs['email_status'] = 'error -> {}'.format(e)

            NotificationLog.objects.create(**log_kwargs)

    class Meta:
        verbose_name = _("Notification")
        verbose_name_plural = _("Notifications")


class NotificationLog(models.Model):
    # Object
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Createdo el"))

    # Notification
    notification = models.ForeignKey('Notification', on_delete=models.CASCADE)

    # Email
    email_status = models.CharField(max_length=250, verbose_name=_("Estado"))

    def __str__(self):
        return "{} -> {} | {}".format(self.content_type.name, self.content_object, self.notification)

    class Meta:
        verbose_name = _("Notification register")
        verbose_name_plural = _("Notification registers")
