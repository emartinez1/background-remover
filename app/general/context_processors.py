from app.general.models import General


def general(request):
    try:
        general = General.get_solo()
        return {'general': general}
    except:
        return {'general': ''}

