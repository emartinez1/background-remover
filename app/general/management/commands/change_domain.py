from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            'domain',
            type=str
        )

    def handle(self, *args, **options):
        domain = options['domain']
        site = Site.objects.first()
        site.domain = domain
        site.name = domain
        site.save()
