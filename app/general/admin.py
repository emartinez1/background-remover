from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group


from solo.admin import SingletonModelAdmin

from app.general.models import General, Notification, NotificationLog

#
# General
#

@admin.register(General)
class GeneralAdmin(SingletonModelAdmin, admin.ModelAdmin):
    pass

admin.site.unregister(Site)
admin.site.unregister(Group)


# @admin.register(Notification)
# class NotificationAdmin(admin.ModelAdmin):
#     fieldsets = (
#         ("", {"fields" : ('action',) }),
#         ("Email", {'classes': ('grp-collapse grp-closed',), "fields" : ('email_active', 'email_subject', 'email_content')})
#     )
# admin.site.register(NotificationLog, admin.ModelAdmin)