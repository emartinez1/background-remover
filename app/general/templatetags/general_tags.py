from django.template import Library
from django.urls import resolve, reverse
from django.utils.translation import activate, get_language
from django.conf import settings
from django.contrib.sites.models import Site

import urllib.parse as urlparse
import uuid
import os

register = Library()


@register.simple_tag()
def get_site_url():
    domain = Site.objects.get_current().domain
    protocol = 'https://'
    if settings.DEBUG:
        protocol = 'http://'
    return protocol + domain


@register.simple_tag()
def get_static_file_version():
    if settings.DEBUG:
        version = "?v=" + str(uuid.uuid1())
    else:
        return "?v=" + str(uuid.uuid1())
    return version


@register.simple_tag(takes_context=True)
def url_parameter(context, name, value):
    path = context.get('request').get_full_path()
    path_parameters = ["{}={}".format(name, value)]

    path_parsed = urlparse.urlparse(path)
    path_parsed_parameters = path_parsed.query.split("&")
    for parameter in path_parsed_parameters:
        parameter_name = parameter.split("=")[0]
        if parameter != '' and parameter_name != name:
            path_parameters += [parameter]
    return path_parsed.path + "?" + "&".join(path_parameters)


@register.simple_tag(takes_context=True)
def translate_url(context, lang):
    active_language = get_language()

    try:
        path = context.get('request').get_full_path()
        # Quit url params for aboid errors
        if "?" in path:
            path = path.split("?")[0]
        path_parts = resolve(path)
        kwargs = path_parts.kwargs

        translated_url = path
        activate(lang)
        translated_url = reverse(path_parts.view_name, kwargs=kwargs)
    except Exception:
        return ""
    finally:
        activate(active_language)

    return translated_url
