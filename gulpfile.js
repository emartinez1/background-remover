
//Require gulp
const gulp = require('gulp');
const pjson = require('./package.json');

var browserSync = require("browser-sync");
var concat = require('gulp-concat');
var csso = require('gulp-csso');
var filter = require('gulp-filter');
var gulpif = require('gulp-if');
var pug = require('gulp-pug');
var pugInheritance = require('gulp-pug-inheritance');
var purgecss = require('gulp-purgecss');
var sassInheritance = require('gulp-sass-inheritance');
var terser = require('gulp-terser');
var sass = require('gulp-sass')(require('sass'));

//Optimize?

const onDjango = true
const pugSmart = false

const optimize = {
  img: true,
  css: false,
  js: true
};
//Folder and File Vars
//this.app = `./${pjson.name}/`
this.app = `./app/`
const rootPaths = {
  src: this.app,
  dst: this.app,
  templates: 'templates/',
  static: 'static/'
};

const parentPaths = {
  templates: {
    src: rootPaths.src + rootPaths.templates,
    dst: rootPaths.dst + rootPaths.templates,
  },
  static: {
    src: rootPaths.src + rootPaths.static,
    dst: rootPaths.dst + rootPaths.static,
  }
};

const paths = {
  templates: {
    dst: parentPaths.templates.dst + 'html/',
    src: parentPaths.templates.src,
    pug: {
      src: parentPaths.templates.src + '**/*.pug',
      srcinc: [parentPaths.templates.src + '_**/*', parentPaths.templates.src + '**/_*'],
      srcco: [parentPaths.templates.src + '**/*.pug', "!" + parentPaths.templates.src + '_**/*', "!" + parentPaths.templates.src + '**/_*'],
    },
    txt: {
      src: parentPaths.templates.src + '**/*.txt'
    },
  },
  static: {
    css: {
      src: parentPaths.static.src + 'css/**/*.css',
      dst: parentPaths.static.dst + 'css/',
    },
    sass: {
      dir: parentPaths.static.src + 'sass/',
      src: parentPaths.static.src + 'sass/**/*.{sass,scss}',
    },
    img: {
      src: parentPaths.static.src + 'img/**/*.{jpg,png,webp,svg}',
      dst: parentPaths.static.dst + 'img/',
    },
    js: {
      app: {
        src: parentPaths.static.src + 'js/app/**/*.js',
        dst: parentPaths.static.dst + 'js/',
      },
      others: {
        src: [parentPaths.static.src + 'js/others/**/*.js', "!" + parentPaths.static.src + 'js/others/**/*-min.js'],
        dst: parentPaths.static.dst + 'js/others/',
      }
    },
    fonts: {
      src: parentPaths.static.src + 'fonts/**/*',
      dst: parentPaths.static.dst + 'fonts/',
    },
    video: {
      src: parentPaths.static.src + 'video/**/*',
      dst: parentPaths.static.dst + 'video/',
    },
  },
};



function style() {
  return gulp.src(paths.static.sass.src, { since: gulp.lastRun(sass) })
    .pipe(sassInheritance({ dir: paths.static.sass.dir }))
    .pipe(filter(function (file) {
      return !/\/_/.test(file.path) && !/^_/.test(file.relative);
    }))
    .pipe(sass.sync({}).on('error', sass.logError))
    
    .pipe(gulpif(optimize.css,
      //-purgecss({ content: [paths.templates.dst + '**/*.html', paths.static.src + 'js/**/*.js'] }),
      csso() //- falta minificar
    ))
    .pipe(gulp.dest(paths.static.css.dst))
    .pipe(browserSync.stream())
};

function allPug() {
  //- compila tots els pugs sense _ a carpeta o fitxer
  return gulp.src(paths.templates.pug.srcco)
    .pipe(pug({ basedir: paths.templates.src }))
    .pipe(gulp.dest(paths.templates.dst))
    .pipe(browserSync.stream())

};

function sincePug() {
  //- allPug + només pasen els fitxers canviats (lastRun).
  return gulp.src(paths.templates.pug.srcco, { since: gulp.lastRun(sincePug) })
    .pipe(pug({ basedir: paths.templates.src }))
    .pipe(gulp.dest(paths.templates.dst))
    .pipe(browserSync.stream())
};

function smartPug() {
  //- sincePug + inheritance(compila pugs afectats pel fitxer canviat [PE en cas de mixin/include])
  return gulp.src(paths.templates.pug.src, { since: gulp.lastRun(smartPug) })
    .pipe(pugInheritance({ basedir: paths.templates.src, skip: 'strange-fix' }))
    .pipe(filter(function (file) {
      return !/\/_/.test(file.path) && !/^_/.test(file.relative);
    }))
    .pipe(pug({ basedir: paths.templates.src }))
    .pipe(gulp.dest(paths.templates.dst))
    .pipe(browserSync.stream())
};

function img() {
  return gulp.src(paths.static.img.src, { since: gulp.lastRun(img) })
    .pipe(gulpif(optimize.img,
      imagemin({ interlaced: true, progressive: true, optimizationLevel: 5, svgoPlugins: [{ removeViewBox: true }] })
    ))
    .pipe(gulp.dest(paths.static.img.dst))
    .pipe(browserSync.stream())
};

function appJs() {
  return gulp.src(paths.static.js.app.src)
    .pipe(concat('app.js'))
    .pipe(gulpif(optimize.js,
      terser()
    ))
    .pipe(gulp.dest(paths.static.js.app.dst))
    .pipe(browserSync.stream())
};

function otherJs() {
  return gulp.src(paths.static.js.others.src, { since: gulp.lastRun(otherJs) })
    .pipe(gulpif(optimize.js,
      terser()
    ))
    .pipe(gulp.dest(paths.static.js.others.dst))
    .pipe(browserSync.stream())
};

function fonts() {
  return gulp.src(paths.static.fonts.src, { since: gulp.lastRun(fonts) })
    .pipe(gulp.dest(paths.static.fonts.dst))
    .pipe(browserSync.stream())
};

function video() {
  return gulp.src(paths.static.video.src, { since: gulp.lastRun(video) })
    .pipe(gulp.dest(paths.static.video.dst))
    .pipe(browserSync.stream())
};

gulp.task('browserSync', function() {
  if (onDjango) {
    browserSync.init(
      {
        // https://www.browsersync.io/docs/options/#option-proxy
        proxy: {
          target: 'localhost:8008',
          proxyReq: [
            function (proxyReq, req) {
              // Assign proxy "host" header same as current request at Browsersync server
              proxyReq.setHeader('Host', req.headers.host)
            }
          ]
        },
        // https://www.browsersync.io/docs/options/#option-open
        //open: false
      }
    )
  } else {
    browserSync.init(
      {
        // https://browsersync.io/docs/options#option-server
        server: {
          baseDir: rootPaths.dst,
          directory: true
        },
        // https://www.browsersync.io/docs/options/#option-open
        open: false
      }
    )
  }
});

function watch() {
  if (pugSmart) {
    gulp.watch(paths.templates.pug.src, smartPug);
  } else {
    gulp.watch(paths.templates.pug.srcco, sincePug);
    gulp.watch(paths.templates.pug.srcinc, allPug);
  }
  gulp.watch(paths.static.sass.src, style);
  // gulp.watch(paths.static.img.src, img);
  gulp.watch(paths.static.js.app.src, appJs);
  gulp.watch(paths.static.js.others.src, otherJs);
  gulp.watch(paths.static.fonts.src, fonts);
  gulp.watch(paths.static.video.src, video);
};

exports.default = gulp.parallel( "browserSync", watch );
exports.rebuild = gulp.parallel(gulp.series(allPug, style), appJs, otherJs, fonts, video);
