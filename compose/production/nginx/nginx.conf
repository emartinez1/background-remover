worker_processes 1;

user nobody nogroup;
error_log  /var/log/nginx/error.log warn;
pid /var/run/nginx.pid;

events {
  worker_connections 1024;
  accept_mutex off;
}

http {
  include mime.types;
  default_type application/octet-stream;
  access_log /var/log/nginx/access.log combined;
  sendfile on;

  upstream app_server {
    server django:8000 fail_timeout=0;
  }

  server {
    listen 80 deferred;

    server_name ${DOMAIN};

    return 301 https://$host$request_uri;
  }

  server {
    listen 443 ssl;
    client_max_body_size 4G;

    server_name ${DOMAIN};

    ssl_certificate /certs/${DOMAIN}.crt;
    ssl_certificate_key /certs/${DOMAIN}.key;

    keepalive_timeout 5;

    location /static/ {
      alias /usr/share/nginx/static/;
    }

    location /media/ {
      alias /usr/share/nginx/media/;
    }

    location / {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto $scheme;
      proxy_set_header Host $http_host;
      proxy_redirect off;
      proxy_pass http://app_server;
    }
  }
}
