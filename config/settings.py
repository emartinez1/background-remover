from django.urls import reverse_lazy
from django.utils.timezone import make_aware
from django.utils.translation import ugettext_lazy as _


import os

from datetime import datetime


#
# General django config
#

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ.get("SECRET_KEY")

DEBUG = os.environ.get("DEBUG")

ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")


INSTALLED_APPS = (
    # Plugins
    'solo',
    'modeltranslation',
    'rosetta',
    'ckeditor',
    'ckeditor_uploader',
    'filebrowser',

    # Django
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    # 'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    # Apps
    'app.users',
    'app.general',
    'app.texts',
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'app/templates/html'),
            'django.template.loaders.app_directories.load_template_source',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'app.general.context_processors.general',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

#
# Databases
#

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get("POSTGRES_DB"),
        'USER': os.environ.get("POSTGRES_USER"),
        'PASSWORD': os.environ.get("POSTGRES_PASSWORD"),
        'HOST': 'postgres',
        'PORT': 5432,
    }
}

DEFAULT_AUTO_FIELD='django.db.models.AutoField' 

#
# Authentification
#

AUTH_USER_MODEL = "users.User"
# LOGIN_REDIRECT_URL = 'stream'
# LOGOUT_REDIRECT_URL = 'login'
# LOGIN_URL = 'login'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

#
# Sessions
#

# SESSION_EXPIRE_AT_BROWSER_CLOSE = True


#
# Internationalization
#

LANGUAGE_CODE = 'en'
TIME_ZONE = 'Europe/Madrid'
USE_I18N = True
USE_L10N = False
USE_TZ = False

TIME_FORMAT='H:i:s'


#
# Static files
#

LANGUAGES = (
    # ('es', _('Español')),
    ('en', _('Inglés')),
)


# Locale

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
]

# Modeltranslation

MODELTRANSLATION_DEFAULT_LANGUAGE = 'en'
# MODELTRANSLATION_FALLBACK_LANGUAGES = ('en', 'es')
MODELTRANSLATION_PREPOPULATE_LANGUAGE = 'en'
MODELTRANSLATION_AUTO_POPULATE = True

STATIC_URL = '/static/'
if DEBUG:
    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, "app/static"),
    ]
else:
    STATIC_ROOT = os.path.join(BASE_DIR, "app/static")

#
# Media
#

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'app/media')





#
# Email
#

EMAIL_HOST = os.environ.get('EMAIL_HOST')
EMAIL_HOST_USER = os.environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_HOST_PASSWORD')
EMAIL_PORT = os.environ.get('EMAIL_PORT')
EMAIL_USE_TLS = os.environ.get('EMAIL_USE_TLS')

TEST_EMAIL = os.environ.get("TEST_EMAIL")
DEFAULT_FROM_EMAIL = os.environ.get("DEFAULT_FROM_EMAIL")



#
# Cache
#

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': 'memcached:11211'
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

#
# Sites
#

SITE_ID = 1


#
# Filebrowser
#

FILEBROWSER_DIRECTORY = 'uploads/'
FILEBROWSER_SHOW_IN_DASHBOARD = False
FILEBROWSER_VERSION_QUALITY = 90
FILEBROWSER_VERSIONS_BASEDIR = '_versions'
FILEBROWSER_EXTENSIONS = {
    'Image': ['.jpg', '.jpeg', '.png'],
    'Icon': ['.svg'],
    'Video': ['.mp4'],
}
FILEBROWSER_VERSIONS = {
    'icon': {'verbose_name': 'Icon', 'width': 32, 'height': 32, 'opts': 'crop'},
    'admin_thumbnail': {'verbose_name': 'Admin Thumbnail', 'width': 60, 'height': 60, 'opts': 'crop'},
    'thumbnail': {'verbose_name': 'Thumbnail ', 'width': 60, 'height': 60, 'opts': 'crop'},
    'user-image': {'verbose_name': 'Thumbnail ', 'width': 100, 'height': 100, 'opts': 'crop'},
    'small': {'verbose_name': 'Small', 'width': 140, 'height': '', 'opts': ''},
    'medium': {'verbose_name': 'Medium', 'width': 300, 'height': '', 'opts': ''},
    'big': {'verbose_name': 'Big', 'width': 460, 'height': '', 'opts': ''},
    'large': {'verbose_name': 'Large', 'width': 680, 'height': '', 'opts': ''},
    'extra_large': {'verbose_name': 'Extra large', 'width': 1920, 'height': '', 'opts': ''},
}
FILEBROWSER_MAX_UPLOAD_SIZE = "429916160000"
FILEBROWSER_DIRECTORY_PATH = os.path.join(MEDIA_ROOT, FILEBROWSER_DIRECTORY)
if not os.path.isdir(FILEBROWSER_DIRECTORY_PATH):
    os.mkdir(FILEBROWSER_DIRECTORY_PATH)

FILE_UPLOAD_PERMISSIONS=0o640


#
#CKeditor 
#

CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor/"
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Basic': [
            {
                'name': _('Basic'),
                'items': ['Format', '-', 'Bold', '-', 'Link', '-', 'NumberedList', 'BulletedList']
            },
        ],
        'format_tags': 'p;h1;h2;h3',
        'toolbar': 'Basic',
    },
    'default_with_image': {
        'toolbar_Basic': [
            {
                'name': _('Basic'),
                'items': ['Format', '-', 'Bold', '-', 'Image', '-', 'Link', '-', 'NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            },
        ],
        'format_tags': 'p;h1;h2;h3',
        'toolbar': 'Basic',
        'extraPlugins': 'uploadimage'
    }
}

#
# Google Analytics
#

GOOGLE_ANALYTICS_KEY = os.environ.get("GOOGLE_ANALYTICS_KEY")

#
# Token code length
#

TOKEN_CODE_LENGTH = 8
