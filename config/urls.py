from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.contrib import admin
from django.conf.urls.i18n import i18n_patterns
from django.views.i18n import set_language

from django.utils.translation import gettext_lazy as _


from filebrowser.sites import site

from app.general.views import error_500

urlpatterns = [
    
    path('ckeditor/', include('ckeditor_uploader.urls')),

    path(
        'admin/filebrowser/', 
        site.urls
    ),

    #i18n
    path(
        'set-language/',
        set_language,
        name='set_language'
    ),
    # Sitemap, robots and humans
    path(
        'humans.txt',
        TemplateView.as_view(
            template_name='humans.txt',
            content_type='text/plain'
        )
    ),
    path(
        'robots.txt',
        TemplateView.as_view(
            template_name='robots.txt',
            content_type='text/plain'
        )
    ),
    #Plugins
    path(
        'rosetta/',
        include('rosetta.urls')
    ),
    #Auth
    path(
        'auth/ask-code/',
        TemplateView.as_view(
            template_name='auth/ask-code.html',
        )
    ),
    path(
        'auth/change-password/',
        TemplateView.as_view(
            template_name='auth/change-password.html',
        )
    ),
    path(
        'auth/change-email/',
        TemplateView.as_view(
            template_name='auth/change-email.html',
        )
    ),
    path(
        'auth/login-1/',
        TemplateView.as_view(
            template_name='auth/login-1.html',
        )
    ),
    path(
        'auth/login-2/',
        TemplateView.as_view(
            template_name='auth/login-2.html',
        )
    ),
    path(
        'auth/register-1/',
        TemplateView.as_view(
            template_name='auth/register-1.html',
        )
    ),
    path(
        'auth/register-2/',
        TemplateView.as_view(
            template_name='auth/register-2.html',
        )
    ),
    path(
        'auth/register-3/',
        TemplateView.as_view(
            template_name='auth/register-3.html',
        )
    ),
    path(
        'auth/payment-method/',
        TemplateView.as_view(
            template_name='auth/payment-method.html',
        )
    ),
    #User Panel
    path(
        'userpanel/cover/',
        TemplateView.as_view(
            template_name='user_panel/cover.html',
        )
    ),
    path(
        'userpanel/invoices/',
        TemplateView.as_view(
            template_name='user_panel/invoices.html',
        )
    ),
    path(
        'userpanel/comunication/',
        TemplateView.as_view(
            template_name='user_panel/comunication.html',
        )
    ),
    path(
        'userpanel/credits/',
        TemplateView.as_view(
            template_name='user_panel/credits.html',
        )
    ),
    path(
        'userpanel/cancel-1/',
        TemplateView.as_view(
            template_name='user_panel/cancel-1.html',
        )
    ),
    path(
        'userpanel/cancel-2/',
        TemplateView.as_view(
            template_name='user_panel/cancel-2.html',
        )
    ),
    path(
        'userpanel/cancel-3/',
        TemplateView.as_view(
            template_name='user_panel/cancel-3.html',
        )
    ),
    path(
        'userpanel/subscription/',
        TemplateView.as_view(
            template_name='user_panel/subscription.html',
        )
    ),
    #Help
    path(
        'help/',
        TemplateView.as_view(
            template_name='help/help-cover.html',
        )
    ),
    path(
        'help/category',
        TemplateView.as_view(
            template_name='help/help-category.html',
        )
    ),
    path(
        'help/category/article/',
        TemplateView.as_view(
            template_name='help/help-article.html',
        )
    ),
    #Others
    path(
        'supermin/',
        TemplateView.as_view(
            template_name='others/supermin.html',
        )
    ),
    path(
        'legal/',
        TemplateView.as_view(
            template_name='others/legal.html',
        )
    ),
    path(
        'email/',
        TemplateView.as_view(
            template_name='others/email.html',
        )
    ),
    #Comercial
    path(
        'comercial/cover',
        TemplateView.as_view(
            template_name='comercial/cover.html',
        )
    ),
    path(
        'comercial/faq',
        TemplateView.as_view(
            template_name='comercial/faq.html',
        )
    ),
    path(
        'comercial/pricing',
        TemplateView.as_view(
            template_name='user_panel/subscription.html',
        )
    ),
    # bgremover
    path(
        'background-remover/cover',
        TemplateView.as_view(
            template_name='background_remover/br.html',
        )
    ),
    # video
    path(
        'video/cover',
        TemplateView.as_view(
            template_name='video/cover.html',
        )
    ),
    path(
        'video/list',
        TemplateView.as_view(
            template_name='video/list.html',
        )
    ),
    path(
        'video/item',
        TemplateView.as_view(
            template_name='video/item.html',
        )
    ),
    path(
        'video/search',
        TemplateView.as_view(
            template_name='video/search.html',
        )
    ),
]

if settings.DEBUG:
    urlpatterns += [
        path('error/', error_500
        )
    ]

urlpatterns += i18n_patterns(
        #Admin
    path(
        'admin/',
        admin.site.urls
    ),
    # Apps
    path(
        '',
        include('django.contrib.auth.urls')
    ),
    path(
        '', 
        include('app.texts.urls')
    ),
) 

# Media
if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )

# Error handlers
handler404 = 'app.general.views.error_404'
handler500 = 'app.general.views.error_500'
handler403 = 'app.general.views.error_403'
handler400 = 'app.general.views.error_400'
