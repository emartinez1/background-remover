# background-remover
##  Requirements

 - Docker [Install guide](https://docs.docker.com/get-docker/)
 - Docker Compose [Install guide](https://docs.docker.com/compose/install/)


# Setup developing environment
Startup docker-compose

```bash
docker-compose up
```

# Setup production enviroment
```bash
docker-compose -f production.docker-compose.yml up
```

# Backup DB

Stored in docker volume background-remover_production_postgres_data_backups

Backup DB
```bash
docker-compose -f production.docker-compose.yml exec postgres backup
```

List backups
```bash
docker-compose -f production.docker-compose.yml exec postgres backups
```

Restore DB
```bash
docker-compose -f production.docker-compose.yml exec postgres restore <backup_file_name>
```

# Gitlab runner

Triggered on TAG update.

```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
```
```bash
export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner
```
```bash
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "emkdo4SRdhds7xTzGMnE" \
  --executor "shell" \
  --docker-image alpine:latest \
  --description "shell runner" \
  --tag-list "shell,development" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
```

Instance URL:
```
https://gitlab.com/
```
Token:
```
emkdo4SRdhds7xTzGMnE
```
Executor:
```
shell
```

Grant runner docker permissions
```bash
sudo usermod -aG docker gitlab-runner
```
Check runner has permissions
```bash
sudo -u gitlab-runner -H docker info
```

Cloned repository:
```
cd /home/gitlab-runner/builds/
```